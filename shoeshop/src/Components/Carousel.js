import React from 'react'
import {UncontrolledCarousel} from 'reactstrap'

export default function Carousel() {
  return (
    <div>
      <UncontrolledCarousel
  items={[
    {
      altText: '',
      caption: '',
      key: 1,
      src: 'https://m.media-amazon.com/images/G/31/img21/shoes/2023/Winterflip/MS/Hero/Men-1._SX3000_QL85_.jpg'
    },
    {
      altText: '',
      caption: '',
      key: 2,
      src: 'https://m.media-amazon.com/images/G/31/img21/shoes/2023/Winterflip/MS/Hero/casual_3000x850._SX3000_QL85_FMpng_.png'
    },
    {
      altText: '',
      caption: '',
      key: 3,
      src: 'https://m.media-amazon.com/images/G/31/img21/shoes/2023/Winterflip/MS/Hero/Men-9._SX3000_QL85_.jpg'
    },
    {
      altText: '',
      caption: '',
      key: 4,
      src: 'https://m.media-amazon.com/images/G/31/img21/shoes/2023/Winterflip/MS/Hero/casual_3000x850._SX3000_QL85_FMpng_.png'
    },
    {
      altText: '',
      caption: '',
      key: 5,
      src: 'https://m.media-amazon.com/images/G/31/img21/shoes/2023/Winterflip/MS/Hero/sandals_3000x850._SX3000_QL85_FMpng_.png'
    },
    {
      altText: '',
      caption: '',
      key: 6,
      src: 'https://m.media-amazon.com/images/G/31/img21/shoes/2023/Winterflip/MS/Hero/Men_pc_deal._SX3000_QL85_.jpg'
    },

  ]}
 />
      
    </div>
  )
}

