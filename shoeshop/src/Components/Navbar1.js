import {React} from 'react'
import logo from '../IMG/logo.png'
import "../Components/Navbar.css"
import { Component } from 'react'

class Navbar1 extends Component{
  state={clicked:false};
  handleClick=()=>{
    this.setState({clicked:!this.state.clicked})
  }
  render(){
  return (
    <div>
         <nav>
    
    <img src={logo} alt="Logo" />


    <div>
        <ul id="navbar" className={this.state.clicked?"#navbar active" : "#navbar"}>
            <li><a className="active" href="/">Home</a></li>
            <li><a href="AboutUs">AboutUs</a></li>
            <li><a href="Registor">Registor</a></li>
            <li><a href="Login">Login</a></li>
            

        </ul>
    </div>
    <div id="mobile" onClick={this.handleClick}>
      <i id="bar" className={this.state.clicked? 'fas fa-times':"fas fa-bars"}></i>


    </div>
    </nav>
    
      
    </div>
  )
}
}

export default Navbar1
