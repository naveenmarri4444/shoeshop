import React, {useRef, useState} from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';


function Register() {
    const nameRef = useRef(null);
    const mobileRef = useRef(null);
    const emailRef = useRef(null);
    const passwordRef = useRef(null);
    const [Msg, setMsg] = useState('');
    const handleRegister = ()=>{
      let newCustomer = {
        "name":nameRef.current.value,
        "mobile": mobileRef.current.value,
        "email":emailRef.current.value,
        'password':passwordRef.current.value
      }
      var c = 0;
      for(let key in newCustomer){
        if(newCustomer[key]===''){
            c+=1;
        }
      }
      if(c<1){
        axios.post('http://127.0.0.1:3003/Register', newCustomer)
        .then((res) => {
            console.log(newCustomer);
        })
        nameRef.current.value ='';
        mobileRef.current.value ='';
        emailRef.current.value ='';
        passwordRef.current.value ='';
        setMsg("Sign Up Successful")
      }
      else{
        setMsg("Enter All Details");
      }
      
    }
  return (
    <div style={{paddingTop:'60px',height:"120vh"}} className='section'>
      <div className='fieldset'>
            <p className='title'><center>Sign Up</center></p>
            <div className='formGroup'>
                <label>Name:</label>
                <input type="text" ref={nameRef} placeholder='Enter Name' required/>
            </div>
            <div className='formGroup'>
                <label>Mobile Number:</label>
                <input type="tel" ref={mobileRef} placeholder='Enter Mobile Number' required />
            </div>
            <div className='formGroup'>
                <label>Email:</label>
                <input type="email" ref={emailRef} placeholder='Enter Email' required/>
            </div>
            <div className='formGroup'>
                <label>Password:</label>
                <input type="password" ref={passwordRef} placeholder='Enter Password' required/>
            </div>
            <div className='formGroup'>
                <button onClick={handleRegister}>Sign Up</button>
                <p>Already have an account <Link to='/login'>Log In</Link></p>
                <p style={{color:'green', fontWeight:'500'}}>{Msg}</p>
            </div>
      </div>
    </div>
  )
}
export default Register;