
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar1 from './Components/Navbar1';
import Home from './Pages/Home';
import Login from '../src/Pages/Login'
import Registor from './Pages/Register'
import AboutUs from './Pages/AboutUs';
 



function App() {
  return (
    <div>

      <Navbar1 />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/AboutUs" element={<AboutUs />} />
          <Route path="/Registor" element={<Registor />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </Router>

     
    </div>
  );
}

export default App;

